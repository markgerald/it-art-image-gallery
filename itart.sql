-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 01/04/2017 às 18:32
-- Versão do servidor: 5.7.17-0ubuntu0.16.04.1
-- Versão do PHP: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `silex`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_users`
--

CREATE TABLE `app_users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `app_users`
--

INSERT INTO `app_users` (`id`, `username`, `password`, `email`, `is_active`) VALUES
(1, 'mark@gerald.eti.br', '$2y$13$1A6E4mNbHk2/Ohcgkw81oOPUGO1GPGyKK68vHE3yZzrNFWHfe0FAS', 'mark@gerald.eti.br', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `file` varchar(45) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `images`
--

INSERT INTO `images` (`id`, `file`, `created_at`, `updated_at`) VALUES
(1, '37165854d0a361445eaca8a54da0bd86.png', '2017-04-01 18:30:01', '2017-04-01 18:30:01'),
(2, 'e8f73dd514b700a702fdafd0b5cbb482.png', '2017-04-01 18:30:05', '2017-04-01 18:30:05'),
(3, '5f1e0a3470d431a8cd4ff0d5dbbb5981.png', '2017-04-01 18:30:09', '2017-04-01 18:30:09'),
(4, 'ebc3e32a635e2ad69fa8c7cd51dd70d1.png', '2017-04-01 18:30:14', '2017-04-01 18:30:14'),
(5, '39c213b134c46ca5a3ce1db36dc5add9.png', '2017-04-01 18:30:31', '2017-04-01 18:30:31'),
(6, '829a2c153444499f32b9de9a1da0c667.png', '2017-04-01 18:30:37', '2017-04-01 18:30:37'),
(7, 'f66825130b49c272bf248fc345a8fbc4.png', '2017-04-01 18:30:43', '2017-04-01 18:30:43'),
(8, '5305fbd913d00fa2bed50c724d161b5d.png', '2017-04-01 18:30:48', '2017-04-01 18:30:48'),
(9, '0782227d669b9b24b5d8d2d4b19838e8.png', '2017-04-01 18:30:52', '2017-04-01 18:30:52'),
(10, '68e6c0c29211da684f6461441d9b0842.png', '2017-04-01 18:30:58', '2017-04-01 18:30:58');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_C2502824F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_C2502824E7927C74` (`email`);

--
-- Índices de tabela `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
