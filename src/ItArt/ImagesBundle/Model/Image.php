<?php
namespace ItArt\ImagesBundle\Model;

use Doctrine\Common\Persistence\ObjectManager;
use ItArt\ImagesBundle\Entity\Images;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Image
 * @package ItArt\ImagesBundle\Model
 */
class Image
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Images $entity
     * @return Images
     */
    public function create(Images $entity) : Images
    {
        $file = $entity->getFile();
        $fileName = $this->container->get('it_art_images.upload.service')->upload($file);
        $entity->setFile($fileName);

        $this->getEm()->persist($entity);
        $this->getEm()->flush();

        return $entity;
    }

    /**
     * @param Images $entity
     * @return Images
     */
    public function update(Images $entity) : Images
    {
        $file = $entity->getFile();
        $fileName = $this->container->get('it_art_images.upload.service')->upload($file);
        $entity->setFile($fileName);
        $this->getEm()->flush();

        return $entity;
    }

    /**
     * @param Images $entity
     */
    public function delete(Images $entity)
    {
        $this->getEm()->remove($entity);
        return $this->getEm()->flush();
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    public function getEm(): ObjectManager
    {
        return $this->container->get('doctrine')->getManager();
    }
}