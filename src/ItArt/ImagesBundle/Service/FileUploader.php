<?php
namespace ItArt\ImagesBundle\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * Class FileUploader
 * @package ItArt\ImagesBundle\Resources
 */
class FileUploader
{
    /**
     * @var string
     */
    private $targetDir;

    /**
     * FileUploader constructor.
     * @param $targetDir
     */
    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file): string
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move($this->targetDir, $fileName);

        return $fileName;
    }

    /**
     * @return string
     */
    public function getTargetDir(): string
    {
        return $this->targetDir;
    }

    /**
     * @param $fileName
     * @return bool|string
     */
    public function removeFile($fileName)
    {
        $fs = new Filesystem();

        try {
            $fs->remove($this->targetDir . "/" . $fileName);
        } catch (IOExceptionInterface $e) {
            return "Ocorreu um erro ao excluir o arquivo ".$e->getPath();
        }

        return false;
    }
}
