<?php

namespace ItArt\ImagesBundle\Controller;

use ItArt\ImagesBundle\Entity\Images;
use ItArt\ImagesBundle\Form\ImagesType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;


/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_list")
     */
    public function indexAction()
    {
        $session = new Session();
        $images = $this->getDoctrine()->getRepository('ItArtImagesBundle:Images')->findAll();

        return $this->render(':it-art/images/admin:index.html.twig', [
            'images' => $images,
            'infoMessage' => $session->getFlashBag()->all()
        ]);
    }

    /**
     * @Route("/new", name="new_image")
     */
    public function newAction(Request $request)
    {
        $image = new Images();
        $form = $this->createForm(ImagesType::class, $image);
        $form->handleRequest($request);
        $session = new Session();

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('it_art_images.model.service')->create($image);
            $session->getFlashBag()->add('success', 'imagem inserida com sucesso!');
            return $this->redirectToRoute('admin_list');
        }

        return $this->render(':it-art/images/admin:new.html.twig', array(
            'form' => $form->createView()

        ));
    }

    /**
     * @Route("/delete/{id}", name="delete_image")
     */
    public function deleteAction($id)
    {
        $image = $this->getDoctrine()->getRepository('ItArtImagesBundle:Images')->find($id);

        if (!$image) {
            throw new HttpException(404, 'Image Not Found');
        }
        $removeFile = $this->get('it_art_images.upload.service')->removeFile($image->getFile());
        $session = new Session();
        if (!$removeFile) {
            $this->get('it_art_images.model.service')->delete($image);
            $session->getFlashBag()->add('success', 'imagem excluída com sucesso!');
        } else {
            $session->getFlashBag()->add('danger', 'Ocorreu um erro ao excluir a imagem!');
        }

        return $this->redirectToRoute('admin_list');
    }
}
