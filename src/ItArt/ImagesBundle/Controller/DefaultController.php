<?php
namespace ItArt\ImagesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $images = $this->getDoctrine()->getRepository('ItArtImagesBundle:Images')->findAll();
        $session = new Session();
        return $this->render(':it-art/images/default:index.html.twig', [
            'images' => $images,
            'infoMessage' => $session->getFlashBag()->all()
        ]);
    }
}
