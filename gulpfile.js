var jsVendor  = [
    './bower_components/jquery/dist/jquery.min.js',
    './bower_components/bootstrap/dist/js/bootstrap.js'
];

var jsApp = [
    './app/Resources/public/js/*.js'
];

var cssApp = ['./app/Resources/public/css/**/*.css'];

var cssVendor = [
    './bower_components/bootstrap/dist/css/bootstrap.css',
];

var gulp = require('gulp');
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var cleanCSS = require('gulp-clean-css');
var htmlmin = require('gulp-htmlmin');

// Tarefa de minificação do Javascript dos Vendors
gulp.task('minify-js', function () {
    return gulp.src(jsVendor)
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./web/js'))
});

// Tarefa de minificação do Javascript do App
gulp.task('minify-js-app', function () {
    gulp.src(jsApp)
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./web/js/'));
});

// Tarefa de minificação do CSS da Vendor
gulp.task('minify-css-vendor', function() {
    return gulp.src(cssVendor)
        .pipe(concat('vendor.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./web/css/'));
});
// Tarefa de minificação do CSS do App
gulp.task('minify-css-app', function() {
    return gulp.src(cssApp)
        .pipe(concat('app.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./web/css/'));
});

gulp.task('js-vendor',['minify-js']);
gulp.task('js-app',['minify-js-app']);
gulp.task('css-vendor',['minify-css-vendor']);
gulp.task('css-app',['minify-css-app']);
gulp.task('default',['minify-js', 'minify-js-app', 'minify-css-vendor', 'minify-css-app']);

gulp.task('watch', function() {
    gulp.watch(jsVendor, ['minify-js']);
    gulp.watch(jsApp, ['minify-js-app']);
    gulp.watch(cssVendor, ['minify-css-vendor']);
    gulp.watch(cssApp, ['minify-css-app']);
});